package org.example.hello.app;

import org.example.hello.Console;
import org.example.hello.Hello;

import java.util.ServiceLoader;

public class App {
    public static void main(String[] args) {
        ServiceLoader<Hello> loader = ServiceLoader.load(Hello.class);
        loader.stream()
                .filter(provider -> provider.type().isAnnotationPresent(Console.class))
                .map(ServiceLoader.Provider::get)
                .forEach(hello -> {
                    // use that instance to say hello.
                    hello.address("world");
                });
    }
}
