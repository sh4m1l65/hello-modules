package edu.two.hello;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HelloApplication extends Application {
    public HelloApplication() {}

    @Override
    public void start(Stage stage) throws Exception {
        String addressee = this.getParameters().getNamed().get("addressee");
        Text message = new Text("\nHello, " + addressee);
        message.setFont(new Font(40));
        Scene scene = new Scene(new Group(message));

        stage.setTitle("Hello from JavaFX!");
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();

    }
}
