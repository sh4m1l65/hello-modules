package edu.two.hello;

import javafx.application.Application;
import org.example.hello.GUI;

@GUI
public class HelloFX implements org.example.hello.Hello {
    @Override
    public void address(String addressee) {
        Application.launch(HelloApplication.class, "--addressee=" + addressee);
    }
}
