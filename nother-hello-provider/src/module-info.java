import org.example.hello.GUI;

module nother.hello.provider {
    exports edu.two.hello;
    provides org.example.hello.Hello with edu.two.hello.HelloFX;

    requires javafx.graphics;

    requires hello.provider.api;
    uses org.example.hello.Hello;
    uses GUI;
}