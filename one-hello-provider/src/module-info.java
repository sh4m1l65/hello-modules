import org.example.hello.Console;

module one.hello.provider {
    exports com.one.hello;
    provides org.example.hello.Hello with com.one.hello.HelloConsole;

    requires hello.provider.api;
    uses org.example.hello.Hello;
    uses Console;
}