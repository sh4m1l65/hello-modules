package com.one.hello;

import org.example.hello.Console;
import org.example.hello.Hello;

@Console
public class HelloConsole implements Hello {
    @Override
    public void address(String addressee) {
        System.out.println("Hello from the console, " + addressee);
    }
}
